<!doctype html>
<html lang="en" class="no-js">
<head>
<meta charset="<?php bloginfo('charset'); ?>">
<!--[if IE]><![endif]-->
<title><?php wp_title( '|', true, 'right' ); ?></title>
<meta name="viewport" content="width=device-width">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDfVPezapdsGSl4NdNU6s85C1UEXu1EIuo&sensor=false"></script>
<?php if ( file_exists(TEMPLATEPATH .'/favicon.ico') ) : ?>
<link rel="shortcut icon" href="<?php bloginfo('template_url'); ?>/favicon.ico">
<?php endif; ?><?php if ( file_exists(TEMPLATEPATH .'/apple-touch-icon.png') ) : ?>
<link rel="apple-touch-icon" href="<?php bloginfo('template_url'); ?>/apple-touch-icon.png">
<?php endif; ?>
<link rel="stylesheet" href="<?php bloginfo( 'template_url' ); ?>/style/css/normalize.css">
<link rel="stylesheet" href="<?php bloginfo( 'template_url' ); ?>/style/css/grid.css">
<link rel="stylesheet" href="<?php bloginfo( 'template_url' ); ?>/style/css/classes.css">
<link rel="stylesheet" href="<?php bloginfo( 'template_url' ); ?>/style/css/main.css">
<link rel="stylesheet" href="<?php bloginfo( 'template_url' ); ?>/style/css/plugins.css">
<link rel="stylesheet" href="<?php bloginfo( 'template_url' ); ?>/style/css/webshop.css">
<link rel="stylesheet" href="<?php bloginfo( 'template_url' ); ?>/style/css/responsive.css">
<link rel="stylesheet" href="<?php bloginfo( 'template_url' ); ?>/style/css/ie.css">
<link href='https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600' rel='stylesheet' type='text/css'>
<?php wp_head(); ?>

<script type="text/javascript">
	function clearText(field) {
		if (field.defaultValue == field.value) field.value = '';
	}
	function restoreText(field) {
		if (field.value == '') field.value = field.defaultValue;
	}

	var site_url = '<?php bloginfo('url'); ?>';

	document.createElement('video');
	document.createElement('audio');
	document.createElement('track');
</script>

<!--[if (gte IE 6)&(lte IE 8)]>
<script type='text/javascript' src="<?php echo get_bloginfo('template_url'); ?>/js/support/respond.min.js"></script>
<![endif]-->
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-176582892-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-176582892-1');
    </script>
</head><?php $body_classes = join( ' ', get_body_class() ); ?>
<!--[if lt IE 7 ]><body class="ie6 <?php echo $body_classes; ?>"><![endif]-->
<!--[if IE 7 ]><body class="ie7 <?php echo $body_classes; ?>"><![endif]-->
<!--[if IE 8 ]><body class="ie8 <?php echo $body_classes; ?>"><![endif]-->
<!--[if IE 9 ]><body class="ie9 <?php echo $body_classes; ?>"><![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--><body class="<?php echo $body_classes; ?>"><!--<![endif]-->

<header id="header" role="banner" class="hide-for-small">
	<div id="top-header" class="bc1">
		<div class="grid">
			<?php if (is_home() || is_front_page()) : ?>
				<h1 id="logo">
					<a href="<?php bloginfo('url'); ?>/">
						<img src="<?php bloginfo('template_url'); ?>/style/images/ui/detol_logo-25.svg" alt="<?php bloginfo('name'); ?>" width="130" />
					</a>
				</h1>
			<?php else : ?>
				<div id="logo">
					<a href="<?php bloginfo('url'); ?>/">
						<img src="<?php bloginfo('template_url'); ?>/style/images/ui/detol_logo-25.svg" alt="<?php bloginfo('name'); ?>" width="130" />
					</a>
				</div>
			<?php endif; ?>

			<div class="top-links right">
				<ul>
    				<?php if (get_option('sumedia_facebook_account')) { ?>
    				<li class="top-links__item top-links__item--facebook">
    				    <a href="<?php echo get_option('sumedia_facebook_account'); ?>" class="facebook" target="_blank"><img src="<?php bloginfo('template_url'); ?>/style/images/ui/facebook.png" /></a>
    				</li>
    				<?php } ?>
					<li>
						<a href="mailto:<?php echo get_option('sumedia_email_company'); ?>" class="fc4 email"><?php echo get_option('sumedia_email_company'); ?></a>
					</li>
					<li>
						<a href="tel:<?php echo get_option('sumedia_telephone_company'); ?>" class="fc4 telephone"><?php echo get_option('sumedia_telephone_company'); ?></a>
					</li>
				</ul>

                <div class="search-bar">
                    <div class="full searchbar form-search">
                        <form role="search" method="get" class="search-form" action="<?php echo home_url( '/' ); ?>">
                            <input type="text" class="search-field" placeholder="Ik ben op zoek naar..." value="<?php echo get_search_query(); ?>" name="s" title="Search" />
                            <input type="hidden" name="post_type" value="product" />
                        </form>
                    </div>
                </div>

			</div>
		</div>
	</div>
	<div id="bottom-header" class="bc3">
		<div class="grid">
			<?php global $woocommerce; ?>
			<a href="<?php echo wc_get_cart_url()?>" class="right" title="<?php _e('Checkout','woothemes') ?>"><img src="<?php bloginfo('template_url'); ?>/style/images/ui/shopping_cart.png" class="shopping-cart" alt="<?php _e('Checkout','woothemes') ?>" /></a>
			<nav id="main-nav" class="main-nav right" role="navigation"><?php wp_nav_menu( array( 'container_class' => 'menu-header', 'theme_location' => 'primary' ) ); ?></nav>
		</div>
	</div>

	<div class="grid">
		<a href="<?php echo get_option('sumedia_facebook_account'); ?>" class="label-swing" target="_blank">
			<div class="pin"></div>
			<img src="<?php bloginfo('template_url'); ?>/style/images/ui/label.png" />
		</a>
	</div>
</header>

<div class="mobile-menu-holder show-for-small">
	<header class="main-header">
		<a href="#" class="open-menu">
			<img src="<?php bloginfo('template_url');?>/style/images/ui/mobile-menu.gif" />
		</a>
		<a href="#" class="close-menu">
			<img src="<?php bloginfo('template_url');?>/style/images/ui/mobile-menu.gif" />
		</a>
		<img src="<?php bloginfo('template_url'); ?>/style/images/ui/detol_logo-25.svg" alt="<?php bloginfo('name'); ?>" class="mob-logo" />
	</header>
	<nav id="mobile-nav" class="mobile-nav" role="navigation"><?php wp_nav_menu( array( 'container_class' => 'menu-header', 'theme_location' => 'primary' ) ); ?></nav>

    <div class="search-bar search-bar--mobile">
        <div class="full searchbar form-search">
            <form role="search" method="get" class="search-form" action="<?php echo home_url( '/' ); ?>">
                <input type="text" class="search-field" placeholder="Ik ben op zoek naar..." value="<?php echo get_search_query(); ?>" name="s" title="Search" />
                <input type="hidden" name="post_type" value="product" />
            </form>
        </div>
    </div>
</div>

<div id="main" role="main">

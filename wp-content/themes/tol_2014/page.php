<?php get_header(); ?>

<section id="main-content">
	<div id="content">
	<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
	
		<?php get_template_part('partials/background'); ?>
		
		<div class="clear"></div>
	
		<article <?php post_class() ?> id="post-<?php the_ID(); ?>">
			<div class="grid">
				<div class="area">
					<header>
						<h2 class="fc1 page-title checkout-titels"><?php the_title(); ?></h2>				
					</header>
					
					<?php the_content(); ?>
				</div>
			</div>
		</article>
	
	<?php endwhile; endif; ?>
	</div>
</section>

<?php get_footer(); ?>
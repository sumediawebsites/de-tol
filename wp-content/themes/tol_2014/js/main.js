/* 

_____/\\\\\\\\\\\________________________________________________________________/\\\_________________________        
 ___/\\\/////////\\\_____________________________________________________________\/\\\_________________________       
  __\//\\\______\///______________________________________________________________\/\\\____/\\\_________________      
   ___\////\\\___________/\\\____/\\\_____/\\\\\__/\\\\\________/\\\\\\\\__________\/\\\___\///____/\\\\\\\\\____     
    ______\////\\\_______\/\\\___\/\\\___/\\\///\\\\\///\\\____/\\\/////\\\____/\\\\\\\\\____/\\\__\////////\\\___    
     _________\////\\\____\/\\\___\/\\\__\/\\\_\//\\\__\/\\\___/\\\\\\\\\\\____/\\\////\\\___\/\\\____/\\\\\\\\\\__   
      __/\\\______\//\\\___\/\\\___\/\\\__\/\\\__\/\\\__\/\\\__\//\\///////____\/\\\__\/\\\___\/\\\___/\\\/////\\\__  
       _\///\\\\\\\\\\\/____\//\\\\\\\\\___\/\\\__\/\\\__\/\\\___\//\\\\\\\\\\__\//\\\\\\\/\\__\/\\\__\//\\\\\\\\/\\_ 
        ___\///////////_______\/////////____\///___\///___\///_____\//////////____\///////\//___\///____\////////\//__

Version: 1.0
Author: Sumedia - Roel Beerens, Gydo Verbeek, Martijn Velders
Author URI: http://www.sumedia.nl

Copyright 2013 - Sumedia

*/

/* ----------------------------------------------------------------
   MOBILE
---------------------------------------------------------------- */

function initMobileMenu() {
	
	$('.main-header a').click(function() {
		
		if($('.mobile-menu-holder').hasClass('open')) {
			$('.mobile-menu-holder').removeClass('open');
			$('#mobile-nav').removeClass('open');
			//$('html').removeClass('noscroll');
			
		} else {
			$('.mobile-menu-holder').addClass('open');
			$('#mobile-nav').addClass('open');
			//$('html').addClass('noscroll');
		}
		
		return false;
		
	});
	
}

/* ----------------------------------------------------------------
   GOOGLE MAPS
---------------------------------------------------------------- */

function initGmaps() {

	$('#map').css({
		'height' : ($(window).height() * .4) + 'px'
	});
	
	var map;
	    
    var tol = new google.maps.LatLng('51.9835469', '5.855623');
    
    var geocoder = new google.maps.Geocoder();

    var MY_MAPTYPE_ID = 'custom_style';
        
    function initialize() {

        var featureOpts = [{
            "featureType": "water",
            "stylers": [{
                "color": "#add3ff"
            }]
        }, {
            "featureType": "administrative.province",
            "stylers": [{
                "visibility": "on"
            }]
        }, {
            "featureType": "administrative.locality",
            "stylers": [{
                "visibility": "off"
            }]
        }, {
            "featureType": "road",
            "elementType": "labels",
            "stylers": [{
                "visibility": "on"
            }]
        }, {
            "featureType": "road",
            "elementType": "labels.text.stroke",
            "stylers": [{
                "visibility": "off"
            }]
        }, {
            "featureType": "poi",
            "stylers": [{
                "visibility": "off"
            }]
        }, {
            "featureType": "transit",
            "stylers": [{
                "visibility": "on"
            }]
        }, {
            "featureType": "administrative.neighborhood",
            "stylers": [{
                "visibility": "off"
            }]
        }];

        var mapOptions = {
            zoom: 15,
            center: new google.maps.LatLng(51.9835469, 5.855623),
            disableDefaultUI: true,
            scrollwheel: false,
            mapTypeControlOptions: {
                mapTypeIds: [google.maps.MapTypeId.ROADMAP, MY_MAPTYPE_ID]
            },
            mapTypeId: MY_MAPTYPE_ID
        };
        
        map = new google.maps.Map(document.getElementById('map'), mapOptions);

        var image = 'http://detol.wcarrot.nl/wp-content/themes/tol_2014/style/images/ui/marker.png';

        var tolMark = new google.maps.Marker({
            position: tol,
            map: map,
            icon: image
        });

        var styledMapOptions = {
            name: 'Custom Style'
        };

        var customMapType = new google.maps.StyledMapType(featureOpts, styledMapOptions);

        map.mapTypes.set(MY_MAPTYPE_ID, customMapType);
        
    }
    
    $(window).resize(function() {
		$('#map').css({
			'height' : ($(window).height() * .4) + 'px'
		});
	});

    google.maps.event.addDomListener(window, 'load', initialize);
	google.maps.event.addDomListener(window, "resize", function() {
		var center = map.getCenter();
		google.maps.event.trigger(map, "resize");
		map.setCenter(center); 
	});
	
}

/* ----------------------------------------------------------------
   FUNCTIONS
---------------------------------------------------------------- */

function initHome() {

	var $sliderHeight = $('.home-content').height() + 165;

	$('.flexslider').flexslider({
		namespace: "flex-",             //{NEW} String: Prefix string attached to the class of every element generated by the plugin
		selector: ".slides > li",       //{NEW} Selector: Must match a simple pattern. '{container} > {slide}' -- Ignore pattern at your own peril
		animation: "slide",              //String: Select your animation type, "fade" or "slide"
		easing: "swing",               //{NEW} String: Determines the easing method used in jQuery transitions. jQuery easing plugin is supported!
		direction: "horizontal",        //String: Select the sliding direction, "horizontal" or "vertical"
		reverse: false,                 //{NEW} Boolean: Reverse the animation direction
		animationLoop: true,             //Boolean: Should the animation loop? If false, directionNav will received "disable" classes at either end
		smoothHeight: false,            //{NEW} Boolean: Allow height of the slider to animate smoothly in horizontal mode  
		startAt: 0,                     //Integer: The slide that the slider should start on. Array notation (0 = first slide)
		slideshow: true,                //Boolean: Animate slider automatically
		slideshowSpeed: 4000,           //Integer: Set the speed of the slideshow cycling, in milliseconds
		animationSpeed: sliderSpeed,            //Integer: Set the speed of animations, in milliseconds
		 
		// Primary Controls
		controlNav: false,               //Boolean: Create navigation for paging control of each clide? Note: Leave true for manualControls usage
		directionNav: true,             //Boolean: Create navigation for previous/next navigation? (true/false)
		prevText: "Previous",           //String: Set the text for the "previous" directionNav item
		nextText: "Next"               //String: Set the text for the "next" directionNav item

	});
	
	$('.scrolldown').click(function() {
		$('html, body').animate({ 
			scrollTop: $('.home-products-holder').offset().top
		}, {
			duration: 800
		});
	});

	$('.slick-slider .slides').slick({
		infinite: true,
		slidesToShow: 1,
		slidesToScroll: 1,
		speed: 600,
		nextArrow: "#slick-next-custom",
		prevArrow: "#slick-prev-custom",
        autoplay: true,
        autoplaySpeed: 4000
	});
	
}


function responsiveSliderHeight(){
	
	
	
	if($(window).width() < 1400 ){
	
		$varHeight = $(window).height() / 3;
		
		$('.flexslider, .flexslider .slides > li').css({
			'height' : 'auto',
		});
	
	} else{
	
		$varHeight = $(window).height() / 2;
		
		$('.flexslider, .flexslider .slides > li').css({
			'height' : 'auto',
		});
		
	}
	
	
}

function initAnimate() {
	
	$('#logo').delay(200).animate({
		'top' : '0px'
	},{
		duration: 1000,
		easing: 'easeOutBack',
		complete: function() {
			$(this).css({
				'top' : '-1px'
			});
			
			$('.social-holder li').each(function (i) {
				$(this).delay(250*i).queue(function(){
				    $(this).addClass('open').dequeue();
				});
			});
			
			$(".social-holder li").hover(function() {
			    $(this).css({
				    'width' : '80px'
			    });
			}, function() {
			    $(this).css({
				    'width' : '65px'
			    });
			});
		}
	});
	
}

function initNews() {

	// $('#news-content').imagesLoaded( function(){
		$('#news-content-actual').isotope({
		    itemSelector: 'article',
			layoutMode: 'masonry',
			resizable: false,
			masonry: {
				columnWidth: $('#news-content-actual').width() / 4
		    }
		}).isotope('insert', $('#news-content').find('article'));
	// });
	
	$(window).smartresize(function(){
		$('#news-content-actual').isotope({
			masonry: { columnWidth: $('#news-content-actual').width() / 4 }
		});
	});

}

function initCatSlider(){

	$('.cat-navigation header').click(function() {


		if ( $('.cat-navigation ul').hasClass('open')){

			$('.cat-navigation ul').slideUp({
				duration: 1500,
                easing: "easeOutQuad"
			});
			$('.cat-navigation ul').removeClass('open');

		} else{

			$('.cat-navigation ul').slideDown({
				duration: 1500,
                easing: "easeOutQuad"
			});
			$('.cat-navigation ul').addClass('open');

		}

	});
}

function initProductshome() {
	
	$('.page-list .image-round').on('click', function() {
		$('.hidden-content.show').show();
		var $dataContent = $(this).parent().attr('data-content');
		var $dataTitle = $(this).parent().attr('data-title');
		var $dataImage = $(this).parent().attr('data-image');
		var $dataVideo = $(this).parent().attr('data-video');
		
		$('.hidden-content.hide').css({
			'visibility' : 'visible'
		});
	
		$('.hidden-content.hide').find('h4').html($dataTitle);
		$('.hidden-content.hide').find('.home-content-product').html($dataContent);
		$('.hidden-content.hide').find('.button-holder a').html('producten').attr('href', site_url + '/producten');
		
		if ( $(window).width() <= 768) {
			var $heightHidden = $('.hidden-content.hide').outerHeight(true) + 300;
		} else{
			var $heightHidden = $('.hidden-content.hide').outerHeight(true);
		}
		
		$('.hidden-content.show').animate({
			height: $heightHidden + 'px'
		}, {
			duration: 800,
			easing: 'easeInBack',
			complete: function() {
				if ( $(window).width() < 768) {
				$('html, body').animate({
					
						scrollTop: $('.hidden-content').offset().top  - 50
				},{
					duration: 800
				});
				} else{
				$('html, body').animate({
					
						scrollTop: $('.hidden-content').offset().top
				},{
					duration: 800
				});
				}
			}
		});
		
		$('.hidden-content.show').children().fadeOut(500, function() {
			$(this).find('h4').html($dataTitle);
			$(this).find('.home-content-product').html($dataContent);
			$(this).find('.button-holder a[data-type="webshop"]').attr('href', site_url + '/product-categorie/'+$dataTitle);
			var videoHolder = $(this).find('.video-holder.pr');
			if($dataVideo != ""){
				$(videoHolder).find('iframe.youtube-id').fadeOut('fast');
				$(videoHolder).addClass('play');
				$(videoHolder).find('img.no-video').fadeIn(800).attr('src',$dataImage);
				$(this).on('click','.play',function(){
					$(videoHolder).removeClass('play');
					$(videoHolder).find('img.no-video').hide();
					$(videoHolder).find('iframe.youtube-id').attr('src','http://www.youtube.com/embed/'+$dataVideo+'?autoplay=1&autohide=1&border=0&wmode=opaque&rel=0&enablejsapi=1&rel=0').fadeIn(800);
				});
				
			} else{
				$(videoHolder).removeClass('play');
				$(videoHolder).find('iframe.youtube-id').fadeOut('fast');
				$(videoHolder).find('img.no-video').attr('src',$dataImage).fadeIn(800);
			}
			$(this).css({
				'visibility' : 'visible'
			}).fadeIn(500);
		});
		
		
		
		/*
		$('.hidden-content .video-js').fadeOut(500, function() {
			$("video:nth-child(1)").attr('src', $dataVideo);
			$("#video1 .vjs-poster").css('background-image', 'url(' + $dataImage + ')').show();
			if ($dataVideo != '') {
				$(this).fadeIn(800);
				$('.no-video').fadeOut('fast');
			} else {
				$('.no-video').attr('src', $dataImage);
				$('.no-video').fadeIn(800);
			}
		});
		*/
		
		/* $('.hidden-content').fadeOut(500, function() {
			$(this).find('h4').html($dataTitle);
			$(this).find('.home-content-product').html($dataContent);
			$(this).find('.button-holder a').attr('href', site_url + '/producten');
			$(this).slideDown();
		}); */
		
		$('.page-list .image-round').not(this).removeClass('active');
		$(this).addClass('active');
	});
	
}

function initVideo() {
	videojs("video", { "controls": true, "autoplay": false, "preload": "auto" });
}

function initMasonry() {
	
	$('.footer-links').isotope({
	    itemSelector: 'article',
		layoutMode: 'masonry',
		resizable: false,
		masonry: {
			columnWidth: ($('.footer-links').width() / 4)
	    }
	}).isotope('insert', $('.footer-links').find('article'));
	
	$(window).smartresize(function(){
		$('.footer-links').isotope({
			masonry: { 
				columnWidth: ($('.footer-links').width() / 4)
			}
		});
	});
	
}

function initSameItemHeight(){

	$('.products li').matchHeight();

}

function getGridSize() {
	return (window.innerWidth < 479) ? 1 :
	(window.innerWidth < 768) ? 2 :
	(window.innerWidth < 960) ? 3 :
	(window.innerWidth < 1280) ? 4 :
	(window.innerWidth > 1279) ? 4 : 5;
}
		
function homepageSlider() {
	
	$('#home-slider-product').flexslider({
		useCSS: false, 
		animation: "slide",
		animationLoop: false,
		slideshow: false,
		itemWidth: 1,
		itemMargin: 15,
		minItems: getGridSize(),
		maxItems: getGridSize(),
		slideshowSpeed: 3000,
		animationSpeed: 1500,
		easing: "easeInOutSine",
		controlNav: false,
		move: 1
	});
	
}

function initFancybox() {
    
    $('a.zoom').fancybox({
        padding: 0,
        helpers:  {
            title:  null
        }
    });
    
}	
		
/* ----------------------------------------------------------------
   SITE
---------------------------------------------------------------- */

$(document).ready(function() {
	initHome();
	homepageSlider();
	responsiveSliderHeight();
	initMobileMenu();
	initAnimate();
	initNews(); 
	initProductshome();
	initSameItemHeight();
	initMasonry();
	initCatSlider();
	initFancybox();
	
	if(document.getElementById('video') !== null) {
		initVideo();
	}
	
	if(document.getElementById("map-container") !== null) {
		initGmaps();
	}
	
	$('.back-top').click(function() {
		$('html, body').animate({
			scrollTop: 0
		},{
			duration: 800
		});
	});
	
	$("#billing_postcode").val("");
	
	$('[placeholder]').focus(function() {
	  var input = $(this);
	  if (input.val() == input.attr('placeholder')) {
	    input.val('');
	    input.removeClass('placeholder');
	  }
	}).blur(function() {
	  var input = $(this);
	  if (input.val() == '' || input.val() == input.attr('placeholder')) {
	    input.addClass('placeholder');
	    input.val(input.attr('placeholder'));
	  }
	}).blur().parents('form').submit(function() {
	  $(this).find('[placeholder]').each(function() {
	    var input = $(this);
	    if (input.val() == input.attr('placeholder')) {
	      input.val('');
	    }
	  })
	});
});

$( window ).resize(function() {
	responsiveSliderHeight();
	
	
	
	if(document.getElementById("home-slider-product") !== null) {
		 var gridSize = getGridSize();
			 
		$('#home-slider-product').data('flexslider').vars.minItems = gridSize;
		$('#home-slider-product').data('flexslider').vars.maxItems = gridSize;
	}
			
});
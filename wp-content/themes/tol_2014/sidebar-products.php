<div class="cat-navigation">
	<?php
    	
		$taxonomy     = 'product_cat';
		$orderby      = 'name';  
		$show_count   = 0;      // 1 for yes, 0 for no
		$pad_counts   = 0;      // 1 for yes, 0 for no
		$hierarchical = 1;      // 1 for yes, 0 for no  
		$title        = '';  
		$empty        = 0;
	  
		$args = array(
			'taxonomy'     => $taxonomy,
			'orderby'      => $orderby,
			'show_count'   => $show_count,
			'pad_counts'   => $pad_counts,
			'hierarchical' => $hierarchical,
			'title_li'     => $title,
			'hide_empty'   => $empty
		);
		
	$all_categories = get_categories( $args );
	
	global $wp_query;
	
	$cat_obj = $wp_query->get_queried_object();
	$terms = $cat_obj->term_id;
	
	if($cat_obj->parent != 0) {
    	$terms = $cat_obj->parent;
	}
	
	if(is_single()) {
        $term = get_the_terms( $post->ID, 'product_cat' );
        foreach ($term as $t) {
            $current_term = $t->term_id;
            $current_parent_term = $t->parent;
        }
        //echo $product->get_categories( ', ', '<span class="posted_in">' . _n( 'Category:', 'Categories:', $size, 'woocommerce' ) . ' ', '.</span>' );
    }
	
	echo '<header><h3 class="fc1">Categorie</h3></header>';

	echo '<ul>';

	foreach ($all_categories as $cat) {
		if($cat->category_parent == 0) {
			$category_id = $cat->term_id;
			$category = get_category( get_query_var( 'product_cat' ) );
//			$cat_id = $category->slug;
			?>
			
			<li class="main-cat <?php echo  ($terms == $cat->term_id ? "selected" : "")  ?> <?php echo ($current_parent_term == $cat->term_id ? "selected" : ""); ?> "><a href="<?php echo get_term_link($cat->slug, 'product_cat') ?>" class="fc5"><span class="triangle"></span><?php echo $cat->name; ?></a>
			
			<?php
				echo '<ul>';
			
				$args2 = array(
				  'taxonomy'     => $taxonomy,
				  'child_of'     => 0,
				  'parent'       => $category_id,
				  'orderby'      => $orderby,
				  'show_count'   => $show_count,
				  'pad_counts'   => $pad_counts,
				  'hierarchical' => $hierarchical,
				  'title_li'     => $title,
				  'hide_empty'   => $empty
				);
				
				$sub_cats = get_categories( $args2 );
				if($sub_cats) {
					foreach($sub_cats as $sub_category) {
						echo '<li class="sub-cat' . ' ' . ($cat_obj->term_id == $sub_category->term_id ? "selected" : "") . ($current_term == $sub_category->term_id ? "selected" : "") . '"><a href="'. get_term_link($sub_category->slug, 'product_cat') .'" class="fc5">- '.$sub_category->name  . '</a></li>';					
					}
				}
				
				echo '</ul>';
			echo '</li>';
		}
	}

	echo '</ul>';

	?>
</div>
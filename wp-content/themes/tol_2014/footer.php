	</div><!-- END MAIN -->
	<footer id="footer" class="bc1" role="contentinfo">
		<div class="grid">

			<div class="footer-column four-fourth first">
				<div class="quarter left first">
					<header>
						<h5 class="fc1">Contact</h5>
					</header>
					<ul>
						<li><?php echo get_option('sumedia_address_company'); ?></li>
						<li><?php echo get_option('sumedia_zipcode_company'); ?> <?php echo get_option('sumedia_city_company'); ?></li>
						<li><?php echo get_option('sumedia_telephone_company'); ?></li>
						<li><a href="mailto:<?php echo get_option('sumedia_email_company'); ?>"><?php echo get_option('sumedia_email_company'); ?></a></li>
					</ul>
					<ul>
    					<?php if (get_option('sumedia_facebook_account')) { ?>
        				<li class="social-li">
        				    <a href="<?php echo get_option('sumedia_facebook_account'); ?>" class="facebook" target="_blank"><img src="<?php bloginfo('template_url'); ?>/style/images/ui/facebook.png" /></a>
        				</li>
        				<?php } ?>
					</ul>
				</div>

				<div class="quarter left">
					<header>
						<h5 class="fc1">Producten</h5>
					</header>
					<ul class="half left">
						<?php
							$taxonomy     = 'product_cat';
							$orderby      = 'id';
							$show_count   = 0;      // 1 for yes, 0 for no
							$pad_counts   = 0;      // 1 for yes, 0 for no
							$hierarchical = 0;      // 1 for yes, 0 for no
							$title        = '';
							$empty        = 0;

							$args = array(
								'taxonomy'     => $taxonomy,
								'orderby'      => $orderby,
								'show_count'   => $show_count,
								'pad_counts'   => $pad_counts,
								'hierarchical' => $hierarchical,
								'title_li'     => $title,
								'hide_empty'   => $empty,
								'parent'       => '0'
							);

							$maincats = get_categories( $args );
								$catcount = ceil(count($maincats) / 2);
								$counter = 0;

							foreach ($maincats as $cat) {

								if($counter == $catcount){
									echo '</ul><ul class="half left"><li><a href="'. get_term_link( $cat ) .'">'. $cat->name .'</a></li>';
									$counter = 0;
								} else{
									echo '<li><a href="'. get_term_link( $cat ) .'">'. $cat->name .'</a></li>';
								}

								$counter++;
							}
						?>
					</ul>
				</div>

				<div class="quarter left last">
					<header>
						<h5 class="fc1">Webshop</h5>
					</header>
					<ul>
						<?php wp_nav_menu( array( 'container_class' => false, 'theme_location' => 'footer' ) ); ?>
					</ul>
				</div>
			</div>

			<div class="footer-column one-fourth">
				<div class="newsletter-holder">
					<header>
						<h5>Nieuwsbrief</h5>
						<p>Meld u hier aan voor onze nieuwsbrief en blijf op de hoogte</p>
					</header>
					<!-- Begin MailChimp Signup Form -->
					<div id="mc_embed_signup">
					<form action="https://de-tol.us7.list-manage.com/subscribe/post?u=c785c69138f9a05b243946333&amp;id=2e897c8c29" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>

					<div class="mc-field-group">
						<input type="text" value="Organisatie" onfocus="if(this.value=='Organisatie'){this.value=''};" onblur="if(this.value==''){this.value='Organisatie'};" name="ORGANI" class="required organisatie wpcf7-text" id="mce-organisatie">
						<input type="email" value="E-mail adres" onfocus="if(this.value=='E-mail adres'){this.value=''};" onblur="if(this.value==''){this.value='E-mail adres'};" name="EMAIL" class="required email wpcf7-text" id="mce-EMAIL">
						<input type="submit" value="Aanmelden" name="subscribe" id="mc-embedded-subscribe" class="button bc2">
					</div>
						<div id="mce-responses" class="clear">
							<div class="response" id="mce-error-response" style="display:none"></div>
							<div class="response" id="mce-success-response" style="display:none"></div>
						</div>    <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
					    <div style="position: absolute; left: -5000px;"><input type="text" name="b_c785c69138f9a05b243946333_2e897c8c29" tabindex="-1" value=""></div>
					</form>
					</div>
				</div>
				<div class="clear"></div>
			</div>

			<div id="footerbottom" class="full left">
				<p>© De Tol</p>
				<div class="creator"><a href="http://www.sumedia.nl" target="_blank">Sumedia</a></div>
			</div>

		</div>
		<div class="back-top">
			<div class="grid"></div>
		</div>
	</footer>

	<?php get_footer('products'); ?>

	<!-- <div class="footer-bottom">
		<div class="grid">
			<h6 class="fc1 left">&copy; <?php bloginfo('name'); ?></h6>
			<h6 class="fc1 right"><span class="creator">Sumedia</span></h6>
		</div>
	</div> -->

	<?php wp_footer(); ?>
    <!--Start Cookie Script--> <script type="text/javascript" charset="UTF-8" src="//cookie-script.com/s/e1089778349e362b42d4972101c5a8a8.js"></script> <!--End Cookie Script-->
    </body>
</html>

<?php

################################################################################
// Set up menus within the wordpress admin sections
################################################################################

function wp_sumedia_menu() { 		
	// Add a new top-level menu:
	    add_menu_page(__('Site Options', 'sumedia'), __('Site Options', 'sumedia'), 'manage_options', basename(__FILE__) , 'wp_sumedia_admin', '', 9999);
	// Add submenus to the custom top-level menu:
		add_submenu_page(basename(__FILE__), __('Options', 'sumedia'),  __('Options', 'sumedia') , 'manage_options', basename(__FILE__) , 'wp_sumedia_admin');
}
add_action('admin_menu', 'wp_sumedia_menu');

################################################################################
// Init our options
################################################################################

// Options = name, default, label, hint, rules
$sumedia_options = (
	array( 
		array('Contact Details', array(
			array('sumedia_name_company', '', 'Name','Company name',''),
			array('sumedia_address_company', '', 'Address','Street + number',''),
			array('sumedia_zipcode_company', '', 'Zipcode','Zipcode',''),
			array('sumedia_city_company', '', 'City','City',''),
			array('sumedia_telephone_company', '', 'Telephone Number','Telephone number',''),
			array('sumedia_email_company', '', 'E-mail address','E-mail address',''),
			array('sumedia_btw_company', '', 'BTW','BTW number',''),
			array('sumedia_kvk_company', '', 'KvK','KvK number',''),
			array('sumedia_bic_company', '', 'BIC','BIC number',''),
			array('sumedia_bank_company', '', 'Bank','Bank account number',''),
			)
		),
		array('Sale advisor', array(
			array('sumedia_name_advisor', '', 'Name','Sale name',''),
			array('sumedia_telephone_advisor', '', 'Telephone Number','Telephone number',''),
			array('sumedia_email_advisor', '', 'E-mail address','E-mail address',''),
			)
		),
		array('Sale Office', array(
			array('sumedia_name_office', '', 'Office','Office name',''),
			array('sumedia_telephone_office', '', 'Telephone Number','Telephone number',''),
			array('sumedia_email_office', '', 'E-mail address','E-mail address',''),
			)
		),
		array('Social Settings', array(
			array('sumedia_facebook_account', '', 'Facebook Account','Facebook page ID',''),
			array('sumedia_twitter_account', '', 'Twitter Account','Twitter user ID',''),
			)
		),
		array('Site Settings', array(
			array('sumedia_google_analytics', '', 'Google Analytics site ID','UA-XXXXX-X',''),
			array('slider_speed', '', 'Slider Speed','Transition Speed',''),
			array('slider_time', '', 'Slider Time','Transition Time',''),
			)
		)
	)
);
	
foreach($sumedia_options as $section) {
	foreach($section[1] as $option) {
		add_option($option[0], $option[1]);
	}
}

function wp_sumedia_admin_css() {
	?>
	<style type="text/css">
		<!--
			#sumedia_form h3 {
				background: #E3E3E3;
				padding: 12px;
				margin-bottom: 0 !important
			}
			#sumedia_form .sumedia_section {
				border: 1px solid #E3E3E3;
				padding: 0 6px;
				background-color: white;
			}
			#sumedia_form table {				
				margin-top: 0 !important;
				border-collapse: collapse;
				border-bottom: 2px solid #F9F9F9;
			}
			#sumedia_form table td, #sumedia_form table th {
				padding: 12px 6px;
				border-bottom: 1px solid #E3E3E3
			}
			#sumedia_form .message {
				padding: 12px;
				border: 2px dashed #98BFE6;
				background: #EAF2FA;
				line-height: 23px;
				font-weight: bold;
			}
		-->
	</style>
	<?php
}
add_action('admin_head', 'wp_sumedia_admin_css');
	
function wp_sumedia_admin() {

	global $sumedia_options;

	if ($_POST['save_sumedia_options']) {
	
		foreach($sumedia_options as $section) {
			foreach($section[1] as $option) {
				update_option($option[0],stripslashes($_POST[$option[0]]));
			}
		}

		/* Sucess */
		echo '<div id="message" class="updated fade"><p><strong>Theme Options Saved</strong></p></div>';
	}
	?>

	<div class="wrap">
		<h2>Site Options</h2>
		<form method="post" action="admin.php?page=theme-config.php" id="sumedia_form">
			<p class="submit message" style="text-align:right"><span style="float:left">These options control various parts of the theme.</span> <input type="submit" value="Save Changes" name="save_sumedia_options" /></p>
			<?php	
			foreach($sumedia_options as $section) {
				echo '<h3>'.$section[0].'</h3><div class="sumedia_section"><table cellspacing="0" cellpadding="0" class="form-table">';
				foreach($section[1] as $option) {
					echo '<tr valign="top">';
					
					echo '<th><label for="'.$option[0].'">'.$option[2].'</label></th><td>';
					
					if ($option[4]=='yesno') {
						$yes = '';
						$no = '';
						if (get_option($option[0])=='yes') $yes='selected="selected"'; else $no='selected="selected"';
						echo '<select name="'.$option[0].'">
							<option value="yes" '.$yes.'>Yes</option>
							<option value="no" '.$no.'>No</option>
						</select>';
					} elseif ($option[4]=='textarea') {
						echo '<textarea id="'.$option[0].'" name="'.$option[0].'" cols="50" rows="6">'.get_option($option[0]).'</textarea>';
					} else {
						echo '<input type="text" id="'.$option[0].'" name="'.$option[0].'" size="25" value="'.get_option($option[0]).'" />';
					}
					
					if ($option[3]) echo '<br/><span class="setting-description">'.$option[3].'</span>';
					
					echo '</td></tr>';
				}
				echo '</table></div><br class="clear" />';
			}
			?>
			<p class="submit" style="text-align:right"><input type="submit" value="Save Changes" name="save_sumedia_options" /></p>
		</form>
	</div>
	<?php
}
?>
<?php

################################################################################
// Enqueue Scripts
################################################################################

function init_scripts() {
    wp_deregister_script( 'jquery' );
    wp_deregister_script( 'comment-reply' );
    // Register Scripts
    wp_enqueue_script( 'jquery', 'https://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js');
    //wp_register_script( 'comment-reply', get_bloginfo('url') . '/wp-includes/js/comment-reply.js?ver=20090102');
    // Queue Scripts
    wp_enqueue_script('modernizr', get_bloginfo('template_url') . '/js/modernizr-2.6.2.min.js', '', '2.6.2', false);
    //wp_enqueue_script('history', get_bloginfo('template_url') . '/js/jquery.history.js', '', '1.7.1', false);

    //if ( get_option( 'thread_comments' ) ) wp_enqueue_script( 'comment-reply',  get_bloginfo('url') . '/wp-includes/js/comment-reply.js?ver=20090102', 'jquery', '', true );

    //wp_enqueue_script('jquery-ajax', get_bloginfo('template_url') . '/js/ajax.js', 'jquery', '', false);
    wp_enqueue_script('jquery-plugins', get_bloginfo('template_url') . '/js/plugins.js', 'jquery', '', false);
    wp_enqueue_script('jquery-scripts', get_bloginfo('template_url') . '/js/main.js', 'jquery', '', true);

}

/*
function footer_scripts() {
	?>
	<script>!window.jQuery && document.write('<script src="<?php bloginfo('template_url'); ?>/js/jquery-1.8.3.min.js"><\/script>')</script>
	<?php if ($analytics = get_option('sumedia_google_analytics')) : ?><script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', '<?php echo $analytics; ?>', 'auto');
  ga('send', 'pageview');

</script><?php endif; ?><?php
}
*/

function header_scripts() {
	?>
	<script type="text/javascript">
	/* Site variables */
	<?php
		$slidertime = get_option('slider_time');
		if ($sliderspeed = get_option('slider_speed'))
	{ ?>
		var sliderSpeed = <?php echo $sliderspeed; ?>;
		var sliderTime = <?php echo $slidertime; ?>;
	<?php } else { ?>
		var sliderSpeed = 1000;
		var sliderTime = 5000;
	<?php } ?>
	var twitterAccount = "<?php echo get_option('sumedia_twitter_account'); ?>";
	</script>
	<?php
}

if (!is_admin()) add_action('init', 'init_scripts', 0);
//add_action('wp_footer', 'footer_scripts', 10);
add_action('wp_head', 'header_scripts', 10);

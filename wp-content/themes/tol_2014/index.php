<?php get_header(); ?>

<section id="main-content">
	<div id="content" class="news">
		
		<div id="mainbanner">
			<img src="<?php echo bloginfo('template_url'); ?>/style/images/banner-inspiratie.jpg">
		</div>
		<?php if (is_search()) : ?>		
			<h1 class="pagetitle">Search Results: &ldquo;<?php the_search_query(); ?>&rdquo; <?php if (get_query_var('paged')) echo ' &mdash; Page '.get_query_var('paged'); ?></h1>
		<?php endif; ?>
	
		<div id="news-content" style="display: none;">			
			<?php get_template_part('loop'); ?>
		</div>
		
		<div id="news-content-actual" class="news-holder">
		</div>
	</div>
</section>

<?php get_footer(); ?>
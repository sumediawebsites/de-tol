<?php if (is_page(16)) { ?>

    <!--    <div id="map-container">-->
    <!--        <div id="map" class="maps_google" style="width: 100%; height: 100%;"></div>-->
    <!--    </div>-->

    <div class="contactbanner">
        <img src="<?php echo bloginfo('template_url'); ?>/style/images/banner-contact.jpg">
    </div>
<?php } else if (is_front_page()) { ?>

    <div id="background-container" class="slick-slider">
        <!-- <div class="white-overlay"></div> -->
        <a href="#" class="slick-nav-custom slick-prev-custom" id="slick-prev-custom"></a>
        <a href="#" class="slick-nav-custom slick-next-custom" id="slick-next-custom"></a>
        <div class="slides">
            <?php
            $images = get_post_meta(get_the_ID(), 'wpcf-background-image');
            for ($index = 0; $index < count($images); $index++) {
                $image = $images[$index];
                $alttext = $alt[$index];
                $thumb = types_render_field('background-image', array('index' => $index, 'url' => 'true')); ?>

                <div><img src="<?php echo $thumb; ?>" alt="<?php echo $thumb; ?>"/></div>

            <?php } ?>
        </div>
        <a href="#" class="scrolldown bc2"></a>
        <div class="grid static">
            <div class="search-holder">
                <header>
                    <h2 class="fc1"><?php echo types_render_field('background-content', array('output' => 'raw')); ?></h2>
                </header>

                <?php /*
			<div class="search-bar">
				<div class="full searchbar form-search">
					<form role="search" method="get" class="search-form" action="<?php echo home_url( '/' ); ?>">
						<input type="text" class="search-field" placeholder="Ik ben op zoek naar..." value="" name="s" title="Search" />
						<input type="hidden" name="post_type" value="product" />
					</form>
				</div>
			</div>
            */ ?>
            </div>
        </div>


    </div>

<?php } else { ?>

    <div id="page-container">
        <ul>
            <?php
            $images = get_post_meta(get_the_ID(), 'wpcf-background-image');

            if ($images):
                for ($index = 0; $index < count($images); $index++) {
                    $image = $images[$index];
                    $thumb = types_render_field('background-image', array(
                        'index' => $index,
                        'url' => 'true',
                        'size' => 'full-small'
                    )); ?>

                    <?php
                    if ($thumb) {
                        echo '<li style="background-image: url(' . $thumb . '); "></li>';
                    } else {
                        $blogurl = get_bloginfo('template_url');
                        echo '<li style="background-image: url(' . $blogurl . '/style/images/ui/stndhdr.jpg); "></li>';
                    }
                }

            else:
                $blogurl = get_bloginfo('template_url');
                echo '<li style="background-image: url(' . $blogurl . '/style/images/ui/stndhdr.jpg); "></li>';
            endif;

            ?>
        </ul>
        <div class="grid static">

            <?php $backgroundContent = types_render_field('background-content', array('output' => 'raw'));

            if ($backgroundContent): ?>
                <div class="background-content">
                    <?php echo $backgroundContent; ?>
                </div>
            <?php endif; ?>
        </div>
    </div>

<?php } ?>
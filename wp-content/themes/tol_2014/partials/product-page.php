<ul class="page-list">
<?php 
	$i = 0; 
	$args = array(
    	'post_type' => 'page',
		'post_status' => null,
		'order' => 'ASC',
		'orderby' => 'name',
		'posts_per_page' => -1,
		'post_parent' => 10
    );
    
    $loop = new WP_Query( $args );
    while ( $loop->have_posts() ) : $loop->the_post(); 
	
	$thumb_id = get_post_thumbnail_id();
	$thumb_url_array = wp_get_attachment_image_src($thumb_id, '', true);
	$thumb_url = $thumb_url_array[0];
	$videoID = types_render_field('youtube-id', array('output' => 'raw'));
?>

	<li data-title="<?php the_title(); ?>" data-content="<?php the_content(); ?>" data-image="<?php echo $thumb_url; ?>" <?php if ($videoID) { echo 'data-video="'.$videoID.'"'; }else{echo'data-video=""';} ?>>
		<div class="image-round">
		
		<?php
		
		$thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'home-thum' );
		$url = $thumb['0'];

		?>
			<div class="inner-round" style="background-image: url(<?php echo $url; ?>);">
				<h5 class="fc1"><?php the_title(); ?></h5>
			</div>
		</div>
		
	</li>
	
<?php $i++; endwhile; wp_reset_postdata(); ?>

</ul>

<div class="hidden-content show">
	<div class="three-quarter left first">
		<header>
			<h4 class="fc1"></h4>
		</header>
		<div class="home-content-product">
		
		</div>
		<div class="button-holder">
			<a href="" class="button bc2" data-type="webshop">Webshop</a>
			<a href="http://detol.wcarrot.nl/product-categorie/maatwerk-2/" class="button bc2">Maatwerkaanvraag</a>
		</div>
	</div>
	<div class="quarter left video-holder">
				
			<div class="video-holder pr">
				<iframe class="youtube-id" height="300"></iframe>
				<img class="no-video" />
				<div class="play"></div>
			</div>
	</div>
</div>

<div class="hidden-content hide">
	<div class="three-quarter left">
		<header>
			<h4 class="fc1"></h4>
		</header>
		<div class="home-content-product">
		
		</div>
		<div class="button-holder">
			<a href="" class="button bc2"></a>
		</div>
	</div>
	<div class="quarter left video-holder">

	</div>
</div>
<div class="search-bar">
	<div class="full searchbar form-search">
		<form role="search" method="get" class="search-form" action="<?php echo home_url( '/' ); ?>">
			<input type="text" class="search-field" placeholder="Ik ben op zoek naar..." value="<?php echo get_search_query(); ?>" name="s" title="Search" />
			<input type="hidden" name="post_type" value="product" />
		</form>
	</div>
</div>
<ul class="page-list">
<?php 
	$i = 0; 
	$args = array(
    	'post_type' => 'page',
		'post_status' => null,
		'order' => 'ASC',
		'orderby' => 'name',
		'posts_per_page' => -1,
		'post_parent' => 10
    );
    
    $loop = new WP_Query( $args );
    while ( $loop->have_posts() ) : $loop->the_post(); 
	
	$thumb_id = get_post_thumbnail_id();
	$thumb_url_array = wp_get_attachment_image_src($thumb_id, '', true);
	$thumb_url = $thumb_url_array[0];
?>

	<li data-title="<?php the_title(); ?>" data-content="<?php the_content(); ?>" data-image="<?php echo $thumb_url; ?>" data-video="<?php $videoAlive = types_render_field('video-upload', array('output' => 'raw')); if ($videoAlive) { echo $videoAlive; } else { echo ''; } ?>">
		<div class="image-round">
			<div class="inner-round">
				<?php the_post_thumbnail('home-thumb'); ?>
			</div>
		</div>
		<h5 class="fc1"><?php the_title(); ?></h5>
	</li>
	
<?php $i++; endwhile; wp_reset_postdata(); ?>

</ul>

<div class="hidden-content show">
	<div class="three-quarter left first">
		<header>
			<h4 class="fc1"></h4>
		</header>
		<div class="home-content-product">
		
		</div>
		<div class="button-holder">
			<a href="" class="button bc2">Webshop</a>
		</div>
	</div>
	<div class="quarter left video-holder">
		<video id="video1" class="video-js vjs-default-skin vjs-big-play-centered" controls preload="none" width="100%" height="250" poster="" data-setup='{}'>
			<source class="play-holder" src="" type='video/mp4' />
			<source class="play-holder" src="" type='video/ogg' />
		</video>
		<img src="" alt="<?php bloginfo('name'); ?>" class="no-video" />
	</div>
</div>

<div class="hidden-content hide">
	<div class="three-quarter left">
		<header>
			<h4 class="fc1"></h4>
		</header>
		<div class="home-content-product">
		
		</div>
		<div class="button-holder">
			<a href="" class="button bc2"></a>
			
		</div>
	</div>
	<div class="quarter left video-holder">

	</div>
</div>
<?php
/*
Template name: Producten
*/
?>

<?php get_header(); ?>

<section id="main-content">
	<div id="content">
	<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
	
		<?php get_template_part('partials/background'); ?>
		
		<div class="clear"></div>
		
		<div class="home-products-holder">
			<div id="product-page" class="grid">
				<!--
					<header>
						<h2 class="fc2 text-center"><?php _e('Producten'); ?></h2>
					</header>
				-->
				<?php  get_template_part('partials/product-page'); ?>
				<br class="clear" />
			</div>
		</div>
		
	<?php endwhile; endif; ?>
	</div>
</section>

<?php get_footer(); ?>
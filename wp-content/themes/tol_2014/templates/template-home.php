<?php
/*
Template Name: Home
*/
?>
<?php get_header(); ?>

<section id="main-content">
	<div id="content" class="home">
	<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
	
		<?php get_template_part('partials/background'); ?>
		<div class="clear"></div>
		
		<!-- <?php get_template_part('partials/social'); ?> -->
	
		<article <?php post_class() ?> id="post-<?php the_ID(); ?>">
			
			<div class="home-products-holder">
				<div id="product-page" class="grid">
					<!--<header>
						 <h2 class="fc2 text-center"><?php _e('Producten'); ?></h2> 
					</header> -->
					<?php get_template_part('partials/product-page'); ?>
					<br class="clear" />
				</div>
			</div>
			
			<div class="grid">				
				<div class="home-content">
					<div class="one-fifth left">
						<header>
							<h1 class="fc1"><?php the_title(); ?></h1>
							<center><img src="<?php bloginfo('template_url'); ?>/style/images/ui/tol.png" alt="<?php bloginfo('name'); ?>" /></center>					
						</header>
					</div>
					<div class="home-text five-fifth left">
						<?php the_content(); ?>
					</div>
					<div class="clear"></div>
				</div>
			</div>
			
			
			
			<div class="behind-text bc1" style="background-image: url(<?php bloginfo('template_url'); ?>/style/images/ui/behind-textimg.png);">
            	<div class="grid">
            		<header>
						<h2 class="fc4"><?php _e('Meest bekeken'); ?></h2>
					</header>
					
					<div id="home-slider-product" class="slider-holder">
						<ul class="products-grid slides">
							<?php 
							$args = array(
								'post_type' => array('product'),
								'meta_key' => 'post_views_count',
								'post_status' => null,
								'orderby' => 'meta_value',
							    'order' => 'DESC',
								'numberposts' => 8
							);
							
							$subpages = get_posts($args);
							foreach($subpages as $post): 
								setup_postdata($post);
							?>
							
							<li class="item">
								<div class="inner">
							        <a href="<?php the_permalink(); ?>" class="product-image">
							        	<div class="product-image-inner">
								        	<?php the_post_thumbnail('medium'); ?>
							        	</div>
							        </a>
							        
							        <h6 class="product-name">
							        	<?php the_title(); ?>
							        	<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><span class="fc2 small-font"><?php _e('Bekijk details'); ?></span></a>
							        </h6>
							        <div class="actions">
							        	<div class="prices">
											<div class="slider-price">
							            		<span class="big"><?php echo $product->get_price_html(); ?></span>
							            	</div>
							        	</div>
							            <a href="<?php the_permalink(); ?>" title="<?php _e('Bekijk product') ?>" class="button bc2"><span><span class="hide-for-small"><?php _e('Bekijk product') ?></span><span class="show-for-small"><?php _e('Bekijk') ?></span></span></a>
							        </div>
								</div>
							</li>
							<?php endforeach; wp_reset_postdata(); ?>
						</ul>
					</div>
                </div>
            </div>
			
            <div class="behind-text" style="background-image: url(<?php bloginfo('template_url'); ?>/style/images/ui/behind-textimg2.png);">
            	<div class="grid">
					<?php 
					$pageid = 48;
					
					$args = array(
						'post_type' => 'page',
						'post__in' => array( $pageid ),
						'post_status' => null
					);
					
					$loop = new WP_Query( $args );
					while ( $loop->have_posts() ) : $loop->the_post();
					?>
					
					<div class="half left">
						<?php the_post_thumbnail(); ?>
					</div>
					
					<div class="half left">
						<div class="center-content">
							<div class="inner-center">
								<?php the_content(); ?>
								
								<div class="half right button-holder">
									<a class="button first bc2" href="http://www.digibrochure.nl/digibrochure/detol/magazine.html#/spreadview/0/" target="_blank">Bekijk</a>
									<a class="button last bc2" href="http://www.digibrochure.nl/digibrochure/detol/magazine.html#/spreadview/0/" target="_blank">Download</a>
								</div>
							</div>
						</div>
					</div>
					
					<?php endwhile; wp_reset_postdata(); ?>
                </div>
            </div>			
			
		</article>
	
	<?php endwhile; endif; ?>
	</div>
</section>

<?php get_footer(); ?>
<?php
/*
Template name: Actie
*/
?>

<?php get_header(); ?>

<section id="main-content">
	<div id="content">
	<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
	
		<?php get_template_part('partials/background'); ?>
		
		<div class="clear"></div>
	
		<article <?php post_class() ?> id="post-<?php the_ID(); ?>">
			<div class="grid">
				<header>
					<h1 class="seo-title page-title"><?php the_title(); ?></h1>
				</header>

                <?= the_content(); ?>
				
				<ul class="actie-list">
				<?php 
					$args = array(
						'post_type' => 'page',
						'post_status' => null,
						'order' => 'DESC',
						'orderby' => 'menu_order',
						'posts_per_page' => -1,
						'post_parent' => 12
					);
					
					$loop = new WP_Query( $args );
					while ( $loop->have_posts() ) : $loop->the_post(); 
				?>

					<li>
						<div class="quarter left">
							<?php the_post_thumbnail('home-thumb'); ?>
						</div>
							
						<div class="three-quarter left text-left">
							<div class="inner-actie">
								<div class="inner-inner-actie">
									<header>
										<h4 class="fc1"><?php the_title(); ?></h4>
									</header>
									<?php the_content(); ?>
									
									<?php 
									$button1 = types_render_field('url-knop-1', array('output' => 'raw'));
									$buttonTitle1 = types_render_field('titel-knop-1', array('output' => 'raw'));
									$button2 = types_render_field('url-knop-2', array('output' => 'raw'));
									$buttonTitle2 = types_render_field('titel-knop-2', array('output' => 'raw'));
									
									if ($button1) {
									?>
									<a href="<?php echo $button1; ?>" class="button bc2"><?php echo $buttonTitle1; ?></a>
									<?php } else if ($button2) { ?>
									<a href="<?php echo $button2; ?>" class="button bc2"><?php echo $buttonTitle2; ?></a>
									<?php } ?>
								</div>
							</div>
						</div>
					</li>
					
				<?php endwhile; wp_reset_postdata(); ?>

				</ul>
			</div>
		</article>
	
	<?php endwhile; endif; ?>
	</div>
</section>

<?php get_footer(); ?>
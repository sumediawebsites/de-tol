<?php
/*
Template name: Contact
*/
?>

<?php get_header(); ?>

<section id="main-content" class="contactpage">
	<div id="content">
	<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

		<?php get_template_part('partials/background'); ?>

		<div class="clear"></div>

		<article <?php post_class() ?> id="post-<?php the_ID(); ?>">
			<div class="grid">
				<div class="half left">
					<h1 class="seo-title page-title"><?php the_title(); ?></h1>

					<div class="half left">
						<div class="contact-column">
							<h4 class="fc2">Adres</h4>
							<?php echo get_option('sumedia_name_company'); ?><br/>
							<?php echo get_option('sumedia_address_company'); ?><br/>
							<?php echo get_option('sumedia_zipcode_company'); ?> <?php echo get_option('sumedia_city_company'); ?><br/><br/>
							Tel: <?php echo get_option('sumedia_telephone_company'); ?><br/>
							E-mail: <a href="mailto:<?php echo get_option('sumedia_email_company'); ?>"><?php echo get_option('sumedia_email_company'); ?></a><br/><br/>
							BTW: <?php echo get_option('sumedia_btw_company'); ?><br/>
							KvK: <?php echo get_option('sumedia_kvk_company'); ?><br/>
							BIC: <?php echo get_option('sumedia_bic_company'); ?><br/>
							IBAN: <?php echo get_option('sumedia_bank_company'); ?><br/>
						</div>
					</div>

					<div class="half left">
						<?php /*<div class="contact-column">
							<h4 class="fc2">Verkoopadviseur</h4>
							<?php echo get_option('sumedia_name_advisor'); ?><br/>
							Tel: <?php echo get_option('sumedia_telephone_advisor'); ?><br/>
							E-mail: <a href="mailto:<?php echo get_option('sumedia_email_advisor'); ?>"><?php echo get_option('sumedia_email_advisor'); ?></a>
						</div> */?>

						<div class="contact-column">
							<h4 class="fc2">Verkoopkantoor</h4>
							<?php echo get_option('sumedia_name_office'); ?><br/>
							Tel: <?php echo get_option('sumedia_telephone_office'); ?><br/>
							E-mail: <a href="mailto:<?php echo get_option('sumedia_email_office'); ?>"><?php echo get_option('sumedia_email_office'); ?></a>
						</div>
					</div>

				</div>
				<div class="half left contact-form">
					<h4 class="fc2">Contact formulier</h4>
					<?php echo do_shortcode('[contact-form-7 id="30" title="Contactformulier"]'); ?>
				</div>
			</div>
		</article>

	<?php endwhile; endif; ?>
	</div>
</section>

<?php get_footer(); ?>

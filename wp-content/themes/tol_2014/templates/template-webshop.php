<?php
/*
Template name: Webshop
*/
?>

<?php get_header(); ?>

<section id="main-content" class="templ-webshop">
	<div id="content">
	<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
	
		<?php get_template_part('partials/background'); ?>
		
		<div class="clear"></div>
		
		<article <?php post_class() ?> id="post-<?php the_ID(); ?>">
			<div class="grid">
				<?php get_template_part('partials/search'); ?>
			
				<div class="category-grid">

					<ul class="left two-seventh">
					<?php
						$taxonomy     = 'product_cat';
						$orderby      = 'id';  
					  
						$maatwerkArgs = array(
							'taxonomy'     => $taxonomy,
							'orderby'      => $orderby,
							'include'      => '80' // maatwerk include
						);
					
					$maatwerkcat = get_categories( $maatwerkArgs );
						$thumbnail_id = get_woocommerce_term_meta(  $maatwerkcat[0]->term_id, 'thumbnail_id', true );
						$image = wp_get_attachment_url( $thumbnail_id );

					foreach ($maatwerkcat as $indexNr => $cat) { ?>
						<li class="item full first">
							<div class="linkimage">
								<a href="<?php echo get_term_link($cat->slug, 'product_cat'); ?>">
									<img src="<?php echo $image; ?>" alt="<?php echo $cat->name ?>" width="100%" />
									<?php echo $cat->name; ?>
								</a>
							</div>
						</li>
					<?php } ?>

					<!-- BEGIN  -->
					<?php
						$taxonomy     = 'product_cat';
						$orderby      = 'id';  
						$show_count   = 0;      // 1 for yes, 0 for no
						$pad_counts   = 0;      // 1 for yes, 0 for no
						$hierarchical = 1;      // 1 for yes, 0 for no  
						$title        = '';  
						$empty        = 0;
					  
						$args = array(
							'taxonomy'     => $taxonomy,
							'orderby'      => $orderby,
							'show_count'   => $show_count,
							'pad_counts'   => $pad_counts,
							'hierarchical' => $hierarchical,
							'title_li'     => $title,
							'hide_empty'   => $empty,
							'exclude'      => '80' // maatwerk exclude
						);
						
					$all_categories = get_categories( $args );
						
					$i  = 0;
					$l  = 0;
					$kwartjes = 0;
					//print_r($all_categories);
					
					$lastID = count($all_categories) - 1;
					// Replace 54 for $lastID to show latest cat
					$category_id = $all_categories[$lastID]->term_id;
					$thumbnail_id = get_woocommerce_term_meta(  $all_categories[$lastID]->term_id, 'thumbnail_id', true );
					$image = wp_get_attachment_url( $thumbnail_id );
					?>
					
					<!-- <li class="item half left first">
						<div class="linkimage">
							<a href="<?php //echo get_term_link($all_categories[54]->slug, 'product_cat'); ?>">
								<img src="<?php //echo $image; ?>" alt="<?php //echo $all_categories[54]->name ?>" width="100%" />
								<div class="img-title">
									<h4 class="fc4 left"><?php //echo $all_categories[54]->name; ?></h4>
									<span class="right"></span>
								</div>
							</a>
						</div>
					</li> -->

					<?php
					foreach ($all_categories as $indexNr => $cat) {
						if($indexNr == $lastID)
							continue;
					
						if($cat->category_parent == 0) {
							$category_id = $cat->term_id;
							$thumbnail_id = get_woocommerce_term_meta( $cat->term_id, 'thumbnail_id', true );
							$image = wp_get_attachment_url( $thumbnail_id );
							
							
							if($i==0) { ?>
								<li class="item full last">
									<div class="linkimage">
										<a href="<?php echo get_term_link($cat->slug, 'product_cat'); ?>">
											<img src="<?php echo $image; ?>" alt="<?php echo $cat->name ?>" width="100%" />
											<?php echo $cat->name; ?>
										</a>
									</div>
								</li>
							</ul>
							<ul class="cat-items-right">
							<?php } 
							/*
							if($i==1) {	?>
								<li class="item half left last">
									<div class="linkimage">
										<a href="<?php echo get_term_link($cat->slug, 'product_cat'); ?>">
											<div class="sale-banner">
												<img src="<?php bloginfo('template_url'); ?>/style/images/ui/sale-banner.png" alt="sale" />
											</div>
											<img src="<?php echo $image; ?>" alt="<?php echo $cat->name ?>" width="100%" />
											<div class="img-title">
												<h4 class="fc4 left"><?php echo $cat->name; ?></h4>
												<span class="right"></span>
											</div>
										</a>
									</div>
								</li>
							<?php } 
							*/
							if($i>0) {
							
							if($kwartjes%5==4)
								echo '<li class="item left seventh last">';
								
							else
								echo '<li class="item left seventh">';
							?>
								<div class="linkimage">
									<a href="<?php echo get_term_link($cat->slug, 'product_cat'); ?>">
										<div class="imageSize">
											<span><img src="<?php echo $image; ?>" alt="<?php echo $cat->name ?>" style="height: 305px" /></span>
										</div>
										<?php echo $cat->name; ?>
									</a>
								</div>
							<?php
							echo '</li>'; $kwartjes++;
							} $i++; $l++;
						}
					}

					echo '</ul></ul>';

					?>
				</div>
			</div>
		</article>
		
	<?php endwhile; endif; ?>
	</div>
</section>

<?php get_footer(); ?>
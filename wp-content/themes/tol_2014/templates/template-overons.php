<?php
/*
Template name: Over Ons
*/
?>

<?php get_header(); ?>

<section id="main-content">
	<div id="content">
	<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
	
		<?php get_template_part('partials/background'); ?>
		
		<div class="clear"></div>
	
		<article <?php post_class() ?> id="post-<?php the_ID(); ?>">
			<div class="grid">
				<div class="area">
					<div class="text-center">
						<header>
							<h1 class="fc2 page-title"><?php the_title(); ?></h1>
						</header>
					</div>
					<div class="padding text-left">
						<div class="three-quarter left">
							<?php the_content(''); ?>
						</div>
						<div class="quarter left">
							<?php the_post_thumbnail(); ?>
						</div>
					</div>
				</div>
				<?php 
					$args = array(
						'post_type' => 'page',
						'post_status' => null,
						'order' => 'ASC',
						'orderby' => 'menu_order',
						'posts_per_page' => -1,
						'post_parent' => 8
					);
					
					$loop = new WP_Query( $args );
					while ( $loop->have_posts() ) : $loop->the_post(); 
					
					$textLeft = types_render_field('tekst-align', array('raw' => 'true'));
					
					if ($textLeft == 1) {
				?>

					<div class="area text-left">
						<div class="three-quarter left">
							<header>
								<h2 class="fc1"><?php the_title(); ?></h2>
							</header>
							
							<?php the_content(); ?>
						</div>
						<div class="quarter left">
							<?php the_post_thumbnail(); ?>
						</div>
					</div>
					
					<?php } else { ?>
					
					<!-- no-border -->
					<div class="area text-right">
						
						<div class="quarter left">
							<?php the_post_thumbnail(); ?>
						</div>

						<div class="three-quarter left">
							<header>
								<h2 class="fc1"><?php the_title(); ?></h2>
							</header>
							
							<?php the_content(); ?>
						</div>

					</div>
					
					<?php } ?>
					
				<?php endwhile; wp_reset_postdata(); ?>
			</div>
		</article>
	
	<?php endwhile; endif; ?>
	</div>
</section>

<?php get_footer(); ?>
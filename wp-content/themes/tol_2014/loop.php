<?php 
if (have_posts()) : while (have_posts()) : the_post(); 
if ( in_category( 'featured-inspiratie' )) {
	$class = 'featured';
} else {
	$class = 'regular';
}	
?>

	<article <?php post_class($class) ?> id="post-<?php the_ID(); ?>">
		<div class="article-holder">
			<div class="article-icon"></div>
			<?php if ( in_category( 'featured-inspiratie' )) { ?>
				<?php the_post_thumbnail('news-big'); ?>
			<?php } else { ?>
				<?php the_post_thumbnail('news-small'); ?>
			<?php } ?>
		</div>
	</article>
	
<?php endwhile; endif; ?>
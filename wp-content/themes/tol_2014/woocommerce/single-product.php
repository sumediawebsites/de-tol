<?php
/**
 * The Template for displaying all single products.
 *
 * Override this template by copying it to yourtheme/woocommerce/single-product.php
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     1.6.4
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

get_header( 'shop' ); ?>
	<section id="main-content">
	<div id="content" class="product-detail col2-left-layout">
	
	<?php 
	$pageid = 10;
	$args = array(
		'page_id' => $pageid,
		'post_status' => null
	);
	
	$loop = new WP_Query( $args );
	while ( $loop->have_posts() ) : $loop->the_post(); ?>
	<!--
	<div id="single-product-container" class="flexslider">	
		<ul class="slides">
			<?php $thumb = types_render_field('background-image', array('index' => 0, 'url' => 'true')); ?>
			<li style="background-image: url(<?php echo $thumb; ?>)"></li>
		</ul>
	</div>
	-->
	<div id="page-container">	
		<ul>
			<?php 
				$images = get_post_meta(get_the_ID(), 'wpcf-background-image');
				for ($index = 0; $index < count($images); $index++) {
				$image = $images[$index];
//				$alttext = $alt[$index];
				$thumb = types_render_field('background-image', array('index' => $index, 'url' => 'true', 'size' => 'full-small')); ?>
				
					<?php
					if($thumb){
						echo '<li style="background-image: url('. $thumb .'); "></li>';
					} else { 
						$blogurl = get_bloginfo('template_url');
						echo '<li style="'. $blogurl .'/style/images/ui/stndhdr.jpg"></li>'; } ?>
				
			<?php } ?>
		</ul>
		<div class="grid static">
			<div class="background-content">
				<?php echo types_render_field('background-content', array('output' => 'raw')); ?>
			</div>
		</div>
	</div>
	
	<?php endwhile; wp_reset_postdata(); ?>
	
	<div class="grid">
	
	<?php
		/**
		 * woocommerce_before_main_content hook
		 *
		 * @hooked woocommerce_output_content_wrapper - 10 (outputs opening divs for the content)
		 * @hooked woocommerce_breadcrumb - 20
		 */
		do_action( 'woocommerce_before_main_content' );
	?>
	
		<?php get_template_part('partials/search');?>
		
		<div class="col-left sidebar">
			<div class="inner-col">
				<div class="inner">
					<?php get_sidebar('products'); ?>
				</div>
			</div>
		</div>
		
		<div class="col-main">
			<div class="inner">

				<?php while ( have_posts() ) : the_post(); ?>
				
					<?php setPostViews(get_the_ID()); ?>
					
					<?php if ( has_term( 'maatwerk-2', 'product_cat' ) ) { ?>
						<?php wc_get_template_part( 'content', 'single-product-maat' ); ?>
					<?php } else { ?>
						<?php wc_get_template_part( 'content', 'single-product' ); ?>
					<?php } ?>
		
				<?php endwhile; // end of the loop. ?>
		
			</div>
		</div>
		<div class="clear"></div>
	<?php
		/**
		 * woocommerce_after_main_content hook
		 *
		 * @hooked woocommerce_output_content_wrapper_end - 10 (outputs closing divs for the content)
		 */
		do_action( 'woocommerce_after_main_content' );
	?>

	<?php
		/**
		 * woocommerce_sidebar hook
		 *
		 * @hooked woocommerce_get_sidebar - 10
		 */
		do_action( 'woocommerce_sidebar' );
	?>
	</div>
	</div>
	</section>
<?php get_footer( 'shop' ); ?>
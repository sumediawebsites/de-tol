<?php
/**
 * Email Addresses
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates/Emails
 * @version     1.6.4
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

?><table cellspacing="0" cellpadding="0" style="width: 100%; vertical-align: top;" border="0">

	<tr>

		<td valign="top" width="50%">

			<h3 style="font-size: 20px; color: #5c5c5c;"><?php _e( 'Billing address', 'woocommerce' ); ?></h3>

			<p style="font-size: 14px; color: #5c5c5c;"><?php echo $order->get_formatted_billing_address(); ?></p>

		</td>

		<?php if ( get_option( 'woocommerce_ship_to_billing_address_only' ) === 'no' && ( $shipping = $order->get_formatted_shipping_address() ) ) : ?>

		<td valign="top" width="50%">

			<h3 style="font-size: 20px; color: #5c5c5c;"><?php _e( 'Shipping address', 'woocommerce' ); ?></h3>

			<p style="font-size: 14px; color: #5c5c5c;"><?php echo $shipping; ?></p>

		</td>

		<?php endif; ?>

	</tr>

</table>
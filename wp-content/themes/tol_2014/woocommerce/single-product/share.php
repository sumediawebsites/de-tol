<?php
/**
 * Single Product Share
 *
 * Sharing plugins can hook into here or you can add your own code directly.
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     1.6.4
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

?>

<?php do_action( 'woocommerce_share' ); // Sharing plugins can hook into here ?>

<?php 
$tweettext = types_render_field('tweet-text', array('output' => 'raw'));
if($tweettext != null) {
	$tweettext = $tweettext;
} else {
	$tweettext = get_the_title();
}
?>
<div class="share" style="display: none">
<!-- AddThis Button BEGIN -->
<div class="addthis_toolbox addthis_default_style ">
<a class="addthis_button_facebook_like facebook" fb:like:layout="button_count"></a>
<a class="addthis_button_tweet tweet"></a>
<a class="addthis_button_linkedin_counter plus" li:counter="none"></a> 
<a class="addthis_counter addthis_pill_style"></a>
</div>
<script type="text/javascript">
	var addthis_config = {
		"data_track_addressbar":false
	};
</script>
<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-535e75fc25043727"></script>
<script type="text/javascript">
var addthis_share = addthis_share || {}
addthis_share = {
    passthrough : {
        twitter: {
                text: "<?php echo $tweettext; ?>",
                hashtags: "detol"
        }
    },
    url_transforms : {
		shorten: {
			twitter: 'bitly',
			facebook: 'bitly'
		}
	}, 
	shorteners : {
		bitly : {} 
	}
}
</script>
<!-- AddThis Button END -->
</div>
<?php
/**
 * Related Products
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     1.6.4
 *
 * Edited by Martijn Coenen, Sumedia for customized Related Posts
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

global $product, $woocommerce_loop;

$related = wc_get_related_products( $posts_per_page );

if ( sizeof( $related ) == 0 ) return;


/*

*/

$relatedCategories = array();
$terms = get_the_terms( $post->ID, 'product_cat' );
foreach ($terms as $term) {
    $product_cat_id = $term->term_id;
    $currentRelatedCategory = get_metadata('woocommerce_term',$product_cat_id,'relatedcat',true);
	if(!empty($currentRelatedCategory))
	{
		$terms = get_term($currentRelatedCategory, 'product_cat'); 
		$theslug = $terms->slug; 
		$relatedCategories[] = $theslug;
	}		
}

if(count($relatedCategories) <1)
{
	$relatedCategories[] = $terms->slug;
}



$args = array(
		'post_type'			=> 'product',
		'ignore_sticky_posts'  => 1,
		'no_found_rows'        => 1,
        'posts_per_page'	=> $posts_per_page,
		'orderby'			=> $orderby,
        'product_cat' 		=> implode(',',$relatedCategories),
        'post_type'			=> 'product',
        'orderby'			=> 'title',
        'post__not_in'		=> array( $product->id ),
    );

/*

 WooCommerce Origina;

$args = apply_filters( 'woocommerce_related_products_args', array(
	'post_type'            => 'product',
	'ignore_sticky_posts'  => 1,
	'no_found_rows'        => 1,	
	'posts_per_page'       => $posts_per_page,
	'orderby'              => $orderby,
	'post__in'             => $related,
	'post__not_in'         => array( $product->id ),
	//'product_cat'		   => 
) );
*/
$products = new WP_Query( $args );

$woocommerce_loop['columns'] = $columns;


if ( $products->have_posts() ) : ?>
	</div>
	<div class="related products">

		<h2><?php _e( 'Related products', 'woocommerce' ); ?></h2>

		<?php woocommerce_product_loop_start(); ?>

			<?php while ( $products->have_posts() ) : $products->the_post(); ?>

				<?php wc_get_template_part( 'content', 'product' ); ?>

			<?php endwhile; // end of the loop. ?>

		<?php woocommerce_product_loop_end(); ?>

	</div>

<?php endif;

wp_reset_postdata();

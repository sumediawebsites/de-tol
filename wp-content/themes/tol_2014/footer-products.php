<div class="grid">
	<div class="footer-links">
			<?php
            global $wp_query;
            $taxonomy     = 'product_cat';
            $orderby      = 'id';
            $show_count   = 0;      // 1 for yes, 0 for no
            $pad_counts   = 0;      // 1 for yes, 0 for no
            $hierarchical = 1;      // 1 for yes, 0 for no
            $title        = '';
            $empty        = 0;

				$args = array(
					'taxonomy'     => $taxonomy,
					'orderby'      => $orderby,
					'show_count'   => $show_count,
					'pad_counts'   => $pad_counts,
					'hierarchical' => $hierarchical,
					'title_li'     => $title,
					'hide_empty'   => $empty,
					'exclude'	   => '16, 80'
				);
				
			$all_categories = get_categories( $args );
			
			$cat_obj = $wp_query->get_queried_object();
		
			foreach ($all_categories as $cat) {
				if($cat->category_parent == 0) {
					$category_id = $cat->term_id;
					$category = get_category( get_query_var( 'product_cat' ) );
//					$cat_id = $category->slug;
					?>
					
					<article class="left">
						<header>
							<h5 class="fc1"><?php echo $cat->name; ?></h5>
						</header>
						
						<ul>
						<?php
						$args2 = array(
						  'taxonomy'     => $taxonomy,
						  'child_of'     => 0,
						  'parent'       => $category_id,
						  'orderby'      => $orderby,
						  'show_count'   => $show_count,
						  'pad_counts'   => $pad_counts,
						  'hierarchical' => $hierarchical,
						  'title_li'     => $title,
						  'hide_empty'   => $empty
						);
						
						$sub_cats = get_categories( $args2 );
						if($sub_cats) {
							foreach($sub_cats as $sub_category) {
								echo '<li class="sub-cat"><a href="'. get_term_link($sub_category->slug, 'product_cat') .'" class="fc5"> ' . $sub_category->name . '</a></li>';					
							}
						}
						?>
						
						</ul>
					</article>
			<?php }
			}
			?>
	</div>
	<div class="clear"></div>
</div>
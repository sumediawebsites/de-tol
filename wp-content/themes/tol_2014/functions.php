<?php

/* 
add_action('wp_head', 'show_template');
function show_template() {
    global $template;
    echo basename($template);
}  */

function custom_login_css() {
	echo '<link rel="stylesheet" type="text/css" href="'.get_stylesheet_directory_uri().'/style/css/login/login.css" />';
}

add_action('login_head', 'custom_login_css');


if (is_admin()) include_once("includes/theme-config.php");

include_once("includes/theme-enqueue.php");

function show_posts_nav() {
   global $wp_query;
   return ($wp_query->max_num_pages > 1);
}

################################################################################
// Add theme sidebars
################################################################################

if ( function_exists('register_sidebar') ) {
	register_sidebar(array(
	  	'name' => __( 'Primary Widget Area' ),
		'id' => 'primary-widget-area',
        'before_widget' => '<section id="%1$s" class="widget %2$s">',
        'after_widget' => '</section>',
        'before_title' => '<h3 class="widget_title">',
        'after_title' => '</h3>',
	));
}

################################################################################
// Add theme support
################################################################################

add_theme_support( 'automatic-feed-links' );
add_theme_support( 'nav-menus' );
add_theme_support( 'post-thumbnails');
add_theme_support( 'woocommerce' );
// set_post_thumbnail_size( 324, 324, true ); 
add_image_size( 'full', 1920, 1080, true );
add_image_size( 'full-small', 1440, 250, true );
add_image_size( 'home-thumb', 350, 350, false );
add_image_size( 'news-small', 445, 999, false );
add_image_size( 'news-big', 890, 999, false );

/* add_image_size( 'news-small', 445, 291, true );
add_image_size( 'news-big', 890, 585, true ); */

register_nav_menus( array(
	'primary' => __( 'Primary Navigation' ),
    'footer' => __( 'Footer Navigation' )
) );

################################################################################
// Comment removal
################################################################################

/*
// Removes from admin menu
add_action( 'admin_menu', 'my_remove_admin_menus' );
function my_remove_admin_menus() {
    remove_menu_page( 'edit-comments.php' );
}
// Removes from post and pages
add_action('init', 'remove_comment_support');

function remove_comment_support() {
    remove_post_type_support( 'post', 'comments' );
    remove_post_type_support( 'page', 'comments' );
}
// Removes from admin bar
function mytheme_admin_bar_render() {
    global $wp_admin_bar;
    $wp_admin_bar->remove_menu('comments');
}
add_action( 'wp_before_admin_bar_render', 'mytheme_admin_bar_render' );
*/

################################################################################
// Actions + Filters
################################################################################

// Remove links to the extra feeds (e.g. category feeds)
remove_action( 'wp_head', 'feed_links_extra', 3 );
// Remove links to the general feeds (e.g. posts and comments)
remove_action( 'wp_head', 'feed_links', 2 );
// Remove link to the RSD service endpoint, EditURI link
remove_action( 'wp_head', 'rsd_link' );
// Remove link to the Windows Live Writer manifest file
remove_action( 'wp_head', 'wlwmanifest_link' );
// Remove index link
remove_action( 'wp_head', 'index_rel_link' );
// Remove prev link
remove_action( 'wp_head', 'parent_post_rel_link', 10, 0 );
// Remove start link
remove_action( 'wp_head', 'start_post_rel_link', 10, 0 );
// Display relational links for adjacent posts
remove_action( 'wp_head', 'adjacent_posts_rel_link', 10, 0 );
// Remove XHTML generator showing WP version
remove_action( 'wp_head', 'wp_generator' );

function custom_excerpt($text) {
	return str_replace('[...]', ' <a href="'. get_permalink($post->ID) . '" class="more">' . 'Lees meer' . '</a>', $text);
}
add_filter('the_excerpt', 'custom_excerpt');

// Allow HTML in descriptions
$filters = array('pre_term_description', 'pre_link_description', 'pre_link_notes', 'pre_user_description');
foreach ( $filters as $filter ) {
	remove_filter($filter, 'wp_filter_kses');
}

function ellipsis($text, $max=100, $append='&hellip;') {
   if (strlen($text) <= $max) return $text;
   $out = substr($text,0,$max);
   if (strpos($text,' ') === FALSE) return $out.$append;
   return preg_replace('/\w+$/','',$out).$append;
}

function get_excerpt($count){
  $permalink = get_permalink($post->ID);
  $excerpt = get_the_content();
  $excerpt = strip_tags($excerpt);
  $excerpt = substr($excerpt, 0, $count);
  $excerpt = substr($excerpt, 0, strripos($excerpt, " "));
  $excerpt = $excerpt.'... <a href="'.$permalink.'">Lees meer</a>';
  return $excerpt;
}

function sb_woo_remove_reviews_tab($tabs) {

	//unset($tabs['reviews']);
	unset($tabs['description']);

	return $tabs;
}

add_filter( 'woocommerce_product_tabs', 'sb_woo_remove_reviews_tab', 98);

################################################################################
// WOOCOMMERCE FILTERS AND ACTIONS
################################################################################

remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_price', 10 );
remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_meta', 40 );

add_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_price', 25 );


################################################################################
// POST VIEWS
################################################################################

function getPostViews($postID){
    $count_key = 'post_views_count';
    $count = get_post_meta($postID, $count_key, true);
    if($count==''){
        delete_post_meta($postID, $count_key);
        add_post_meta($postID, $count_key, '0');
        return "0";
    }
    return $count;
}
function setPostViews($postID) {
    $count_key = 'post_views_count';
    $count = get_post_meta($postID, $count_key, true);
    if($count==''){
        $count = 0;
        delete_post_meta($postID, $count_key);
        add_post_meta($postID, $count_key, '0');
    }else{
        $count++;
        update_post_meta($postID, $count_key, $count);
    }
}

################################################################################
// WOOCOMMERCE FUNCTIONS
################################################################################

//DISABLE WOOCOMMERCE PRETTY PHOTO SCRIPTS
add_action( 'wp_print_scripts', 'my_deregister_javascript', 100 );

function my_deregister_javascript() {
	wp_deregister_script( 'prettyPhoto' );
	wp_deregister_script( 'prettyPhoto-init' );
}

//DISABLE WOOCOMMERCE PRETTY PHOTO STYLE
add_action( 'wp_print_styles', 'my_deregister_styles', 100 );

function my_deregister_styles() {
	wp_deregister_style( 'woocommerce_prettyPhoto_css' );
}

add_filter( 'woocommerce_cart_item_name', 'add_sku_in_cart', 20, 3);

function add_sku_in_cart( $title, $values, $cart_item_key ) {
    $sku = $values['data']->get_sku();
    return $sku ? $title . sprintf(" (SKU: %s)", $sku) : $title;
} 

// Hook in
add_filter( 'woocommerce_checkout_fields' , 'custom_override_checkout_fields' );

// Our hooked in function - $fields is passed via the filter!
function custom_override_checkout_fields( $fields ) {
     unset($fields['billing']['billing_address_2']);

     return $fields;
}

add_action( 'product_cat_add_form_fields', 'showRelatedCategorySelect');

function showRelatedCategorySelect()
{


	  $taxonomy     = 'product_cat';
	  $orderby      = 'name';  
	  $show_count   = 0;      // 1 for yes, 0 for no
	  $pad_counts   = 0;      // 1 for yes, 0 for no
	  $hierarchical = 1;      // 1 for yes, 0 for no  
	  $title        = '';  
	  $empty        = 0;
	$args = array(
	  'taxonomy'     => 'product_cat',
	  'orderby'      => 'name',
	  'show_count'   => 0,
	  'pad_counts'   => 0,
	  'hierarchical' => 1,
	  'title_li'     => '',
	  'hide_empty'   => 0
	);

	$all_categories = get_categories( $args );
    ?>
    <div class="form-field">
        <label for="term_meta[custom_term_meta]">Kies gerelateerde categorie</label>
        <select name="term_meta[relatedcat]" id="term_meta[relatedcat]">
        	<option value="" selected="selected">Kies een categorie</option>
        	<?php
        	foreach($all_categories as $cat)
        	{
	        	echo '<option value="'.$cat->term_id.'">'.$cat->name.'</option>';
        	}
        	
        	?>
        </select>
        <!--<input type="text" name="term_meta[custom_term_meta]" id="term_meta[custom_term_meta]" value="">-->
        <p class="description">Kies een categorie welke aan deze categorie gekoppeld moet worden. De producten uit de gekozen categorie worden weergegeven onder "gerelateerde producten" van de huidige categorie.</p>
    </div>
	<?php
}

add_action( 'product_cat_edit_form_fields', 'editRelatedCategorySelect', 10, 2 );
function editRelatedCategorySelect($term) {

//print_r($term);

    // put the term ID into a variable
    $t_id = $term->term_id;
	
	$currentRelatedCategory = get_metadata('woocommerce_term',$term->term_id,'relatedcat',true);

    
    $currentSelected = $term_meta['relatedcat'];

	  $taxonomy     = 'product_cat';
	  $orderby      = 'name';  
	  $show_count   = 0;      // 1 for yes, 0 for no
	  $pad_counts   = 0;      // 1 for yes, 0 for no
	  $hierarchical = 1;      // 1 for yes, 0 for no  
	  $title        = '';  
	  $empty        = 0;
	$args = array(
	  'taxonomy'     => 'product_cat',
	  'orderby'      => 'name',
	  'show_count'   => 0,
	  'pad_counts'   => 0,
	  'hierarchical' => 1,
	  'title_li'     => '',
	  'hide_empty'   => 0
	);

	$all_categories = get_categories( $args );
	
    
    ?>
    <!--
    <tr class="form-field">
    <th scope="row" valign="top"><label for="term_meta[custom_term_meta]"><?php _e( 'Example meta field', 'tutorialshares' ); ?></label></th>
        <td>
            <input type="text" name="term_meta[custom_term_meta]" id="term_meta[custom_term_meta]" value="<?php echo esc_attr( $term_meta['custom_term_meta'] ) ? esc_attr( $term_meta['custom_term_meta'] ) : ''; ?>">
            <p class="description"><?php _e( 'Enter a value for this field','tutorialshares' ); ?></p>
        </td>
    </tr>
    -->
    
    
    <tr class="form-field">
    <th scope="row" valign="top"><label for="term_meta[custom_term_meta]"><?php _e( 'Gerelateerde Categorie', 'tutorialshares' ); ?></label></th>
        <td>
        <select name="term_meta[relatedcat]" id="term_meta[relatedcat]">
        
        	<?php
        	if(empty($currentRelatedCategory) )
        	{
	        	echo '<option value="" selected="selected">Kies een categorie</option>';
        	} else {
	        	echo '<option value="">Kies een categorie</option>';
        	}
        	
        	foreach($all_categories as $cat)
        	{
        		if(!empty($currentRelatedCategory) && $currentRelatedCategory == $cat->term_id)
        		{
	        		echo '<option value="'.$cat->term_id.'" selected="selected">'.$cat->name.'</option>';
        		} else {
	        		echo '<option value="'.$cat->term_id.'">'.$cat->name.'</option>';	
        		}
	        	
        	}
        	
        	?>
        </select>
        <!--<input type="text" name="term_meta[custom_term_meta]" id="term_meta[custom_term_meta]" value="">-->
        <p class="description">Kies een categorie welke aan deze categorie gekoppeld moet worden. De producten uit de gekozen categorie worden weergegeven onder "gerelateerde producten" van de huidige categorie.</p>
    </td>
    </tr>


<?php
}

function save_related_category( $term_id ) {
    if ( isset( $_POST['term_meta']['relatedcat'] ) ) {

       $test = update_woocommerce_term_meta( $term_id, 'relatedcat', esc_attr( $_POST['term_meta']['relatedcat'] ) );
       //$test = update_metadata('related_categories',$term_id,'relatedcat',$_POST['term_meta']['relatedcat']);
       if(!$test)
       {
	       $fallback = add_woocommerce_term_meta( $term_id, 'relatedcat', esc_attr( $_POST['term_meta']['relatedcat'] ));
	       //$fallback = add_metadata('related_categories',$term_id,'relatedcat',$_POST['term_meta']['relatedcat']);
	       if(!$fallback)
	       	die('fout');
       }


       
    }
}  
add_action( 'created_term', 'save_related_category', 10, 3 );
add_action( 'edit_term', 'save_related_category', 10, 3 );

################################################################################
// VERZENDKOSTEN; VAST TARIEF VERBERGEN BIJ GRATIS VERZENDING
################################################################################

function hide_all_shipping_when_free_is_available( $available_methods ) {

  	if( isset( $available_methods['free_shipping'] ) ) :

		// Get Free Shipping array into a new array
		$freeshipping = array();
		$freeshipping = $available_methods['free_shipping'];

		// Empty the $available_methods array
		unset( $available_methods );

		// Add Free Shipping back into $avaialble_methods
		$available_methods = array();
		$available_methods[] = $freeshipping;

	endif;

	return $available_methods;
}
add_filter( 'woocommerce_available_shipping_methods', 'hide_all_shipping_when_free_is_available', 10, 1 );

/** to change the position of excerpt **/
remove_action('woocommerce_after_single_product_summary', 'woocommerce_output_product_data_tabs', 10 );
add_action( 'woocommerce_after_single_product_summary', 'woocommerce_output_product_data_tabs', 30 );

################################################################################
// EXPERIMENT
################################################################################

add_filter( 'woocommerce_product_tabs', 'woo_remove_product_tabs', 98 );

function woo_remove_product_tabs( $tabs ) {

    unset( $tabs['additional_information'] );  	// Remove the additional information tab

    return $tabs;

}
		
		remove_action('woocommerce_after_single_product_summary', 'woocommerce_output_product_data_tabs', 10);
<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** URL settings ** //
define( 'WP_ROOT_URL', 'http://de-tol.local' );
define( 'WP_HOME', 'http://de-tol.local' );
define( 'WP_SITEURL', 'http://de-tol.local/wp' ); // Current site URL for running the installation

// ** MySQL settings - You can get this info from your web host ** //
define( 'DB_NAME', 'devsumedia_de_tol_update' );
define( 'DB_USER', 'devsumedia' );
define( 'DB_PASSWORD', '^u;F!Go7y0g;' );
define( 'DB_HOST', 'dev-db.sumedia.nl' );

/**
 * WordPress Localized Language, defaults to English.
 *
 * Change this to localize WordPress. A corresponding MO file for the chosen
 * language must be installed to wp-content/languages. For example, install
 * de_DE.mo to wp-content/languages and set WPLANG to 'de_DE' to enable German
 * language support.
 */
define('WPLANG', 'nl_NL');

// ** Enable development tools ** //
define( 'WP_DEBUG', true ); // Can be true or false